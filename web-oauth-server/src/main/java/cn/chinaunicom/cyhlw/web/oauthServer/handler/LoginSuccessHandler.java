package cn.chinaunicom.cyhlw.web.oauthServer.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 成功处理类，这里有个问题，
 * 因为如果停留在登录界面这时严重中心重启以后，
 * 验证成功以后会找不到跳往哪里，所以这里重新验证成功接口，
 * 找不到跳转以后指定地址处理一下----（暂时没找到更好的办法）
 */
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private RequestCache requestCache = new HttpSessionRequestCache();

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private AuthorizationServerTokenServices authorizationServerTokenServices;

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 被指定的系统主页的地址
     */
    @Value("${security.oauth2.sso.system-index-url}")
    private String systemIndexUrl ;
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response, Authentication authentication)
            throws ServletException, IOException {

            SavedRequest savedRequest = requestCache.getRequest(request, response);

            if (savedRequest == null) { //这里要改造下，不然清空了缓存，或者服务器重启停滞在登录界面的情况会有问题，这里改造成即使拿不到cookie也跳到主页，因为已经通过了密码验证
                clearAuthenticationAttributes(request);
                getRedirectStrategy().sendRedirect(request, response, systemIndexUrl);
                return;
            }
            String targetUrlParameter = getTargetUrlParameter();
            if (isAlwaysUseDefaultTargetUrl()
                    || (targetUrlParameter != null && StringUtils.hasText(request
                    .getParameter(targetUrlParameter)))) {
                requestCache.removeRequest(request, response);
                super.onAuthenticationSuccess(request, response, authentication);

                return;
            }

            clearAuthenticationAttributes(request);

            // Use the DefaultSavedRequest URL
            String targetUrl = systemIndexUrl;////这里只能设定一个主页不然很复杂savedRequest.getRedirectUrl(); //
            logger.debug("Redirecting to DefaultSavedRequest Url: " + targetUrl);
            getRedirectStrategy().sendRedirect(request, response, targetUrl);

    }

    public void setRequestCache(RequestCache requestCache) {
        this.requestCache = requestCache;
    }
}
