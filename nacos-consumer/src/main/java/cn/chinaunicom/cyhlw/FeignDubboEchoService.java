package cn.chinaunicom.cyhlw;

import com.alibaba.cloud.dubbo.annotation.DubboTransported;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2022/3/3.
 *
 * @author wws
 */
//test feign with dubbo,不过似乎没有什么必要
@Component
@FeignClient(name = "nacos-provider", fallback = FeignDubboEchoServiceFallback.class, configuration = FeignDubboConfiguration.class)
@DubboTransported(protocol = "dubbo")
public interface FeignDubboEchoService {
    @RequestMapping(value = "/wwsEcho/{str}")
    public String echo(@PathVariable("p1")String message);
}

class FeignDubboConfiguration {
    @Bean
    public FeignDubboEchoServiceFallback feignDubboEchoServiceFallback() {
        return new FeignDubboEchoServiceFallback();
    }
}

class  FeignDubboEchoServiceFallback implements FeignDubboEchoService {
    @Override
    public String echo(@PathVariable("str") String str) {
        return "wws feign echo fallback";
    }
}