package cn.chinaunicom.cyhlw;

import com.alibaba.cloud.dubbo.annotation.DubboTransported;
import com.alibaba.cyhlw.dubbo.service.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * Created by Administrator on 2022/3/3.
 *
 * @author wws
 */
@Component
@FeignClient("nacos-provider")
@DubboTransported(protocol = "dubbo")
public interface DubboFeignRestService {

    @GetMapping("/param")
    String param(@RequestParam("param") String param);

    @PostMapping("/params")
    String params(@RequestParam("b") String paramB, @RequestParam("a") int paramA);

    @PostMapping(value = "/request/body/map", produces = APPLICATION_JSON_UTF8_VALUE)
    User requestBody(@RequestParam("param") String param,
                     @RequestBody Map<String, Object> data);

    @RequestMapping("/headers")
    String headers(@RequestHeader("h2") String header2,
                   @RequestParam("v") Integer value, @RequestHeader("h") String header);

    @GetMapping("/path-variables/{p1}/{p2}")
    String pathVariables(@RequestParam("v") String param,
                         @PathVariable("p2") String path2, @PathVariable("p1") String path1);

}