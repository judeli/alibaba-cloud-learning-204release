package cn.chinaunicom.cyhlw;

import com.alibaba.cloud.dubbo.annotation.DubboTransported;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2022/3/2.
 *
 * @author wws
 */
@RestController
@RequestMapping("/test")
public class TestDubboClient {

    /*原生DUBBO调用*/
    @DubboReference/*(version = "1.0.0",group = "wws-dev")*/
   com.alibaba.cyhlw.dubbo.service.EchoService dubooEchoService;
    @RequestMapping("/dubboEcho")
    public String echo(){
        return dubooEchoService.echo("wws-test-dubbo");
    }

    /*feign http调用*/
    @Autowired
    FeignEchoService  feignEchoService;
    @RequestMapping("/feignEcho")
    public String feignecho(){
        return feignEchoService.echo("wws-test-feign-consumer");
    }

    /*dubbo feign 调用实列*/
    @Autowired
    @Lazy
    private DubboFeignRestService dubboFeignRestService;

    private void callRequestBodyMap() {

        Map<String, Object> data = new HashMap<>();
        data.put("id", 1);
        data.put("name", "小马哥");
        data.put("age", 33);

        // Dubbo Service call
//        System.out.println(restService.requestBodyMap(data, "Hello,World"));
        // Spring Cloud Open Feign REST Call (Dubbo Transported)
        System.out.println(dubboFeignRestService.requestBody("Hello,World", data));
        // Spring Cloud Open Feign REST Call
        // System.out.println(feignRestService.requestBody("Hello,World", data));

        // RestTemplate call
//        System.out.println(restTemplate.postForObject(
//                "http://" + providerApplicationName + "/request/body/map?param=小马哥", data,
//                User.class));
    }

    @RequestMapping("/dubboFeignEcho")
    public String dubboFeignEcho(){
        callRequestBodyMap();
        return "yes";
    }

    @RequestMapping("/dubboFeignHeader")
    public String dubboFeignHeader(){
        System.out.println(dubboFeignRestService.headers("Hello,World,header",111,"wws"));
        return "yes";
    }

    //wws write
    @Autowired
    FeignDubboEchoService feignDubboEchoService;
    @RequestMapping("/dubboFeignWwsEcho")
    public String dubboFeignWwsEcho(){
        return feignDubboEchoService.echo("Hello,World,wws echo");
    }
//    @Bean
//    @LoadBalanced
//    @DubboTransported
//    public RestTemplate restTemplate() {
//        return new RestTemplate();
//    }
}
