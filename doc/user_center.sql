/*
Navicat MySQL Data Transfer

Source Server         : 10.41.15.22
Source Server Version : 50731
Source Host           : 10.41.15.22:3306
Source Database       : user_center

Target Server Type    : MYSQL
Target Server Version : 50731
File Encoding         : 65001

Date: 2022-03-11 17:46:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for client_details
-- ----------------------------
DROP TABLE IF EXISTS `client_details`;
CREATE TABLE `client_details` (
  `app_Id` varchar(128) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `app_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(128) DEFAULT NULL,
  `grant_types` varchar(128) DEFAULT NULL,
  `redirect_url` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `auto_approve_scopes` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`app_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for code_detail
-- ----------------------------
DROP TABLE IF EXISTS `code_detail`;
CREATE TABLE `code_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0' COMMENT '乐观锁版本',
  `parent_id` int(11) DEFAULT NULL COMMENT '父节点id',
  `has_child` char(1) DEFAULT NULL COMMENT '是否有子节点',
  `code` char(20) DEFAULT NULL,
  `code_val` char(30) DEFAULT NULL,
  `seq` int(11) DEFAULT '0' COMMENT '排序',
  `enabled` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_parent_id` (`parent_id`),
  KEY `USER_CENTER_IDX_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0',
  `table_name` varchar(20) DEFAULT NULL,
  `opt_type` char(10) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `ref_id` int(11) DEFAULT '-1',
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_ref_id` (`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_access_token`;
CREATE TABLE `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` blob,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------
DROP TABLE IF EXISTS `oauth_approvals`;
CREATE TABLE `oauth_approvals` (
  `user_id` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `scope` varchar(128) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `last_modified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_client
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client`;
CREATE TABLE `oauth_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(64) DEFAULT NULL COMMENT '分配的客戶端ID',
  `client_secret` varchar(128) DEFAULT NULL COMMENT '分配的客戶端密碼',
  `scope` varchar(20) DEFAULT NULL COMMENT '范围',
  `grant_type` varchar(30) DEFAULT NULL COMMENT '授权类型',
  `web_server_redirect_uri` varchar(150) DEFAULT NULL COMMENT '跳转地址',
  `access_token_validity` varchar(30) DEFAULT NULL COMMENT 'token有效期',
  `additional_information` varchar(255) DEFAULT NULL,
  `auto_approve` char(1) DEFAULT '0' COMMENT '自动认证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_client_id` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(128) NOT NULL COMMENT '分配的客戶端ID',
  `client_secret` varchar(128) DEFAULT NULL COMMENT '分配的客戶端密碼',
  `authorized_grant_types` varchar(128) DEFAULT NULL,
  `scope` varchar(20) DEFAULT NULL COMMENT '范围',
  `web_server_redirect_uri` varchar(150) DEFAULT NULL COMMENT '跳转地址',
  `access_token_validity` int(11) DEFAULT NULL COMMENT 'token有效期',
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(255) DEFAULT NULL,
  `autoapprove` varchar(128) DEFAULT '0' COMMENT '自动认证',
  `resource_ids` varchar(128) DEFAULT NULL,
  `authorities` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`client_id`),
  UNIQUE KEY `USER_CENTER_IDX_client_id` (`client_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_token`;
CREATE TABLE `oauth_client_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication_id` varchar(128) NOT NULL,
  `user_name` varchar(128) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------
DROP TABLE IF EXISTS `oauth_code`;
CREATE TABLE `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------
DROP TABLE IF EXISTS `oauth_refresh_token`;
CREATE TABLE `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` blob,
  `authentication` blob
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for priv
-- ----------------------------
DROP TABLE IF EXISTS `priv`;
CREATE TABLE `priv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0' COMMENT '乐观锁版本',
  `priv_name` varchar(20) NOT NULL COMMENT '权限名称',
  `priv_desc` varchar(20) NOT NULL COMMENT '权限描述',
  `enabled` char(1) DEFAULT '0',
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `mod_user_id` int(11) DEFAULT NULL,
  `mod_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_priv_name` (`priv_name`),
  KEY `USER_CENTER_IDX_create_user_id` (`create_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for priv_has_resource
-- ----------------------------
DROP TABLE IF EXISTS `priv_has_resource`;
CREATE TABLE `priv_has_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `priv_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `enabled` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_priv_id` (`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0' COMMENT '乐观锁版本',
  `parent_id` int(11) DEFAULT NULL COMMENT '上级ID',
  `label` varchar(150) DEFAULT NULL COMMENT '资源名',
  `type` char(20) DEFAULT NULL COMMENT '资源类型',
  `btn_permission` varchar(30) DEFAULT NULL COMMENT '按钮权限名称',
  `tooltip` varchar(30) DEFAULT NULL COMMENT '网页提示',
  `uri` varchar(150) DEFAULT NULL COMMENT '资源链接',
  `priority` int(11) DEFAULT NULL COMMENT '优先权',
  `css` varchar(100) DEFAULT NULL COMMENT '自定义样式',
  `lay_is_open` char(1) DEFAULT '0' COMMENT '是否展开',
  `enabled` char(1) DEFAULT '0',
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `mod_user_id` int(11) DEFAULT NULL,
  `mod_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_parent_id` (`parent_id`),
  KEY `USER_CENTER_IDX_type` (`type`),
  KEY `USER_CENTER_IDX_create_user_id` (`create_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0' COMMENT '乐观锁版本',
  `role_name` varchar(20) NOT NULL COMMENT '角色名字',
  `role_desc` varchar(20) NOT NULL COMMENT '角色的中文名字',
  `enabled` char(1) DEFAULT '0',
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `mod_user_id` int(11) DEFAULT NULL,
  `mod_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_role_name` (`role_name`),
  KEY `USER_CENTER_IDX_create_user_id` (`create_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for role_has_priv
-- ----------------------------
DROP TABLE IF EXISTS `role_has_priv`;
CREATE TABLE `role_has_priv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `priv_id` int(11) DEFAULT NULL,
  `enabled` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_has_call_line
-- ----------------------------
DROP TABLE IF EXISTS `user_has_call_line`;
CREATE TABLE `user_has_call_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `call_line_type` char(30) DEFAULT NULL,
  `call_type` char(1) DEFAULT NULL,
  `enabled` char(1) DEFAULT '0',
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `mod_user_id` int(11) DEFAULT NULL,
  `mod_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_user_id` (`user_id`),
  KEY `USER_CENTER_IDX_create_user_id` (`create_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_has_priv
-- ----------------------------
DROP TABLE IF EXISTS `user_has_priv`;
CREATE TABLE `user_has_priv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `priv_id` int(11) DEFAULT NULL,
  `enabled` char(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_has_resource
-- ----------------------------
DROP TABLE IF EXISTS `user_has_resource`;
CREATE TABLE `user_has_resource` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` char(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_has_role
-- ----------------------------
DROP TABLE IF EXISTS `user_has_role`;
CREATE TABLE `user_has_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `enabled` char(1) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version` int(11) DEFAULT '0' COMMENT '乐观锁版本',
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `head_img_url` varchar(150) DEFAULT NULL COMMENT '头像URL',
  `password` varchar(128) DEFAULT NULL COMMENT '密码',
  `enabled` char(1) DEFAULT '0',
  `supper` char(1) DEFAULT '0',
  `manager_id` int(11) DEFAULT NULL,
  `create_user_id` int(11) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT NULL,
  `mod_user_id` int(11) DEFAULT NULL,
  `mod_time` timestamp NULL DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `USER_CENTER_UNI_IDX_id` (`id`),
  KEY `USER_CENTER_IDX_name` (`name`),
  KEY `USER_CENTER_IDX_manager_id` (`manager_id`),
  KEY `USER_CENTER_IDX_username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
