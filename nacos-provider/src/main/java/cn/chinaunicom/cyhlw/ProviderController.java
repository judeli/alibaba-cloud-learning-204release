package cn.chinaunicom.cyhlw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2022/3/3.
 *
 * @author wws
 */
@RequestMapping("/nacos")
@RestController
public class ProviderController {

    @Value("${server.port}")
    private String port;
    @Value("${spring.application.name}")
    private String name;
    @Autowired
    private DiscoveryClient discoveryClient;
    @RequestMapping("/info")
    public String getInfo() {
        return name + "-" + port;
    }

    @GetMapping("/discovery")
    public Map<String, List<ServiceInstance>> discovery() {
// Get the list of service names
        List<String> servicesList = discoveryClient.getServices();
// According to the service name Get Under every service name Information on various services
        Map<String, List<ServiceInstance>> map = new HashMap<>();
        servicesList.stream().forEach(service -> {
            map.put(service, discoveryClient.getInstances(service));
        });
        return map;
    }
}
