package cn.chinaunicom.cyhlw;

import com.alibaba.cyhlw.dubbo.service.EchoService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Administrator on 2022/3/3.
 *
 * @author wws
 */
@DubboService/*(version = "1.0.0",group = "wws-dev")*/
public class EchoServiceImpl implements EchoService {
    @Override
    @RequestMapping("/wwsEcho/{p1}")
    public String echo(@PathVariable("p1")String message) {
        return "Hello " + message;
    }

}