package cn.chinaunicom.cyhlw.facade.userCenter.service;

import com.baomidou.mybatisplus.extension.service.IService;
import cn.chinaunicom.cyhlw.facade.userCenter.entity.UserInfo;

/**
 * Created by Administrator on 2022/3/10.
 *
 * @author wws
 */
public interface IUserService extends IService<UserInfo> {

    UserInfo findUserByUserName(String username);

//    List<String> queryLogoutUrlByclientId(String clientId);
    public UserInfo create(String username, String password);
}