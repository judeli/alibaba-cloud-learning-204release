package cn.chinaunicom.cyhlw.web.oauthServer.support.security;

import cn.chinaunicom.cyhlw.facade.userCenter.entity.UserInfo;
import cn.chinaunicom.cyhlw.facade.userCenter.service.IUserService;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author wws
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Value("${spring.profiles.active}")
    String profiles;

    @Reference
    IUserService userService;

//    @Reference
//    IUserHasCallLineService userHasCallLineService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfo user = userService.findUserByUserName(username);


        if (user == null) {
            throw new BadCredentialsException("USER_NOT_FIND");
        }
        //查询用户按钮权限 这里包括了两条线的权限（只包括按钮权限--btnpermission）
//        List<Resource> resources = userService.querybtnPermissionByUserName(username);
//        user.setResources(resources);
//        List<UserHasCallLine> userHasCallLines = userHasCallLineService.getUserHasCallLineByUserId(user.getId());
//        user.setUserHasCallLines(userHasCallLines);
        logger.info("find user {}",user.getUsername());
        return new LoginUserInfo(user);
    }

}


