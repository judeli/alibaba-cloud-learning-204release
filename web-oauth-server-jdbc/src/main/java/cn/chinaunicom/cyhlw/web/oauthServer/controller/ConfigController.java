package cn.chinaunicom.cyhlw.web.oauthServer.controller;

import cn.chinaunicom.cyhlw.facade.userCenter.entity.UserInfo;
import cn.chinaunicom.cyhlw.facade.userCenter.service.IUserService;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * Created by Administrator on 2022/3/1.
 *
 * @author wws
 */
@RestController
@RefreshScope
public class ConfigController {

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @Reference
    IUserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    Logger logger = LoggerFactory.getLogger(ConfigController.class);

    /*获取用户信息接口*/
    @RequestMapping(value = "/user/me", method = RequestMethod.GET)
    public Principal getUser(Principal principal) {
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>");
        logger.info(principal.toString());
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>");
        return principal;
    }

    @RequestMapping("/config/get")
    public boolean get() {
        return useLocalCache;
    }

    @RequestMapping(value = "/registry", method = RequestMethod.POST)
    public boolean createUser(@RequestParam("username") String username, @RequestParam("password") String password) {
        UserInfo ui = new UserInfo();
        ui.setUsername(username);
        ui.setPassword(passwordEncoder.encode(password));
        return userService.save(ui);
    }
}
