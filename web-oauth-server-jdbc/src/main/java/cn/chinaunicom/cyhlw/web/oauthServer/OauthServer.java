package cn.chinaunicom.cyhlw.web.oauthServer;


import org.apache.catalina.filters.RequestDumperFilter;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;

/**
 * Created by Administrator on 2017/12/1.
 *
 * @author wws
 */
@SpringBootApplication(exclude = {
//        ElasticsearchDataAutoConfiguration.class,
//        ElasticsearchRestClientAutoConfiguration.class,
//        RedisAutoConfiguration.class,
        KafkaAutoConfiguration.class
})
@ComponentScan(basePackages = "cn.chinaunicom.cyhlw")
@EnableDubbo(scanBasePackages = "cn.chinaunicom.cyhlw")
@EnableScheduling
//@MapperScan({"cn.chinaunicom.cyhlw.web.oauth.server.mapper"})
/*注解开启资源服务，因为程序需要对外暴露获取token的API和验证token的API所以该程序也是一个资源服务器*/
//@EnableResourceServer
@EnableDiscoveryClient
public class OauthServer {

//    @Bean
//    RequestDumperFilter requestDumperFilter() {
//        return new RequestDumperFilter();
//    }
//
//
//    /**
//     * 通过这个restTemplate 可以进行各服务器之间的带Token
//     * @param factory
//     * @return
//     */
//    @Bean
//    public OAuth2RestTemplate restTemplate(UserInfoRestTemplateFactory factory) {
//        return factory.getUserInfoRestTemplate();
//    }

    public static void main(String[] args) {
        try {
            SpringApplication.run(OauthServer.class, args);
        }catch(Throwable e) {
            e.printStackTrace();
        }
    }
}
