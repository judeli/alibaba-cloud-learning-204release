package cn.chinaunicom.cyhlw.facade.userCenter.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.gitee.sunchenbin.mybatis.actable.annotation.Column;
import com.gitee.sunchenbin.mybatis.actable.annotation.Index;
import com.gitee.sunchenbin.mybatis.actable.annotation.Table;
import com.gitee.sunchenbin.mybatis.actable.annotation.Unique;
import com.gitee.sunchenbin.mybatis.actable.constants.MySqlTypeConstant;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Administrator on 2022/3/10.
 *
 * @author wws
 */
@Data
@Accessors(chain = true)
@Table(name = "user_info")
public class UserInfo {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    @Unique
    @Column(name = "id",type = MySqlTypeConstant.INT,isKey = true,isAutoIncrement = true)
    private Integer id;
    @Column(name = "version",type = MySqlTypeConstant.INT,defaultValue = "0",comment = "乐观锁版本")
    private Integer version;
    @Index
    @Column(name = "name",type = MySqlTypeConstant.VARCHAR,length = 50)
    private String name;
    @Index
    @Column(name = "username",type = MySqlTypeConstant.VARCHAR,length = 50,comment = "用户名")
    private String username;
    /*
    头像
     */
    @Column(name = "head_img_url",type = MySqlTypeConstant.VARCHAR,length = 150,comment = "头像URL")
    private String headImgUrl;

    @Column(name = "password",type = MySqlTypeConstant.VARCHAR,length = 50,comment = "密码")
    private String password;

    /**
     * 是否禁用
     */
    @Column(name = "enabled",type = MySqlTypeConstant.CHAR,length = 1,defaultValue = "0")
    private boolean enabled;

    /**
     * 是否超级用户
     */
    @Column(name = "supper",type = MySqlTypeConstant.CHAR,length = 1,defaultValue = "0")
    private boolean supper;

    /**
     * 上级ID，简单实现上下级关系
     */
    @Index
    @Column(name = "manager_id",type = MySqlTypeConstant.INT)
    private Integer managerId;

    @Column(name = "create_user_id",type = MySqlTypeConstant.INT)
    private Integer createUserId;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time",type = MySqlTypeConstant.TIMESTAMP)
    private Date createTime;
    @Column(name = "mod_user_id",type = MySqlTypeConstant.INT)
    private Integer modUserId;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "mod_time",type = MySqlTypeConstant.TIMESTAMP)
    private Date modTime;
    @Column(name = "remark",type = MySqlTypeConstant.VARCHAR)
    private String remark;

}
