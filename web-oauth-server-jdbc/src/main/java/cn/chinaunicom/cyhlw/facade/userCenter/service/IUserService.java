package cn.chinaunicom.cyhlw.facade.userCenter.service;

import cn.chinaunicom.cyhlw.facade.userCenter.entity.UserInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * Created by Administrator on 2022/3/10.
 *
 * @author wws
 */
public interface IUserService extends IService<UserInfo> {

    UserInfo findUserByUserName(String username);

//    List<String> queryLogoutUrlByclientId(String clientId);
    public UserInfo create(String username, String password);
}