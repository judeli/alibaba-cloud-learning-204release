package cn.chinaunicom.cyhlw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * Created by Administrator on 2022/3/2.
 *
 * @author wws
 */
@SpringBootApplication
@EnableDiscoveryClient
public class NacosConfigApplication {

    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(NacosConfigApplication.class, args);
//        ConfigurableApplicationContext applicationContext = SpringApplication.run(NacosConfigApplication.class, args);
//
//        while(true) {
//            //When configurations are refreshed dynamically, they will be updated in the Enviroment, therefore here we retrieve configurations from Environment every other second.
//            String userName = applicationContext.getEnvironment().getProperty("user.name");
//            String userAge = applicationContext.getEnvironment().getProperty("user.age");
//            System.err.println("user name :" + userName + "; age: " + userAge);
//
//            String currentEnv = applicationContext.getEnvironment().getProperty("current.env");
//            System.err.println("in "+currentEnv+" enviroment; ");
//
//            String global_test = applicationContext.getEnvironment().getProperty("global-test");
//            System.err.println("global_test :"+global_test);
//
//            TimeUnit.SECONDS.sleep(5);
//        }
    }
}
